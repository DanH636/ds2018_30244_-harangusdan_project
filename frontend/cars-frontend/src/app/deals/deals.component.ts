import {Component, OnInit} from '@angular/core';
import {DealsService} from '../services/deals/deals.service';
import {LoginService} from '../services/login/login.service';
import {AddCarService} from '../services/add-car/add-car.service';
import {User} from '../models/User';
import {Car} from '../models/Car';
import {CarDeal} from '../models/CarDeal';

@Component({
  selector: 'app-deals',
  templateUrl: './deals.component.html',
  styleUrls: ['./deals.component.css']
})
export class DealsComponent implements OnInit {

  user: User;
  car: Car;
  offerAmount: number;

  constructor(private dealsService: DealsService,
              private loginService: LoginService,
              private carService: AddCarService) {
  }

  ngOnInit() {
    this.loginService.getUser(localStorage.getItem('username'), localStorage.getItem('password'))
      .subscribe(user => {
        this.user = user;
        this.carService.getCar(localStorage.getItem('dealCar')).subscribe(car => {
          this.car = car;
        });
      });
  }

  addCarDeal() {
    const carDeal = new CarDeal();
    carDeal.ownerUser = this.car.user;
    carDeal.dealUser = this.user;
    carDeal.carId = this.car;
    carDeal.dealAmount = this.offerAmount;
    this.dealsService.putCarDeal(carDeal).subscribe();
  }
}
