import {Component, OnInit} from '@angular/core';
import {LoginService} from '../services/login/login.service';
import {AddCarService} from '../services/add-car/add-car.service';
import {CommentService} from '../services/comment/comment.service';
import {User} from '../models/User';
import {Car} from '../models/Car';
import {Comments} from '../models/Comments';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {

  user: User;
  car: Car;
  comment: string;

  constructor(private loginService: LoginService,
              private carService: AddCarService,
              private commentService: CommentService) {
  }

  ngOnInit() {
    this.loginService.getUser(localStorage.getItem('username'), localStorage.getItem('password'))
      .subscribe(user => {
        this.user = user;
        this.carService.getCar(localStorage.getItem('dealCar')).subscribe(car => {
          this.car = car;
        });
      });
  }

  addCarComment() {
    const comment = new Comments();
    comment.userId = this.user;
    comment.carId = this.car;
    comment.comment = this.comment;
    this.commentService.putComment(comment).subscribe();
  }
}
