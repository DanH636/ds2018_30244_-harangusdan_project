import {User} from './User';

export class Car {
  model: string;
  year: number;
  price: number;
  description: string;
  favorite: number;
  user: User;
}
