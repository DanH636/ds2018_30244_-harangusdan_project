import {Car} from './Car';
import {User} from './User';

export class Comments {
  comment: string;
  userId: User;
  carId: Car;
}
