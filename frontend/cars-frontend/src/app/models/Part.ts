import {Car} from './Car';
import {User} from './User';

export class Part {
  part: string;
  price: number;
  car: Car;
  description: string;
  user: User;
  favorite: number;
}
