import {User} from './User';
import {Car} from './Car';

export  class CarDeal {
  ownerUser: User;
  dealUser: User;
  dealAmount: number;
  carId: Car;
}
