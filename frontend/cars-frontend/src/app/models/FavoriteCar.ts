import {User} from './User';
import {Car} from './Car';

export class FavoriteCar {
  car: Car;
  user: User;
}
