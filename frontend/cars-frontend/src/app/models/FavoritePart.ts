import {Part} from './Part';
import {User} from './User';

export class FavoritePart {
  part: Part;
  user: User;
}
