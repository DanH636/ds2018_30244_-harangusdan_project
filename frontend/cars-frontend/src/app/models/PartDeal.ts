import {User} from './User';
import {Part} from './Part';

export class PartDeal {
  ownerUser: User;
  dealUser: User;
  dealAmount: number;
  partId: Part;
}
