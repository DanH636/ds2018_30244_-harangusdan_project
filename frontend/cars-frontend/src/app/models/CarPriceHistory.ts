import {Car} from './Car';

export class CarPriceHistory {
  carId: Car;
  price: number;
}
