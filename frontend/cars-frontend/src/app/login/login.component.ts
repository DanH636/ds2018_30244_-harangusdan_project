import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from '../services/login/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  title = 'Cars and Car-parts deals';
  username: string;
  password: string;
  @Output() log = new EventEmitter<boolean>();

  constructor(private router: Router,
              private loginService: LoginService) {
  }

  ngOnInit() {
  }

  logUser() {
    this.loginService.getUser(this.username, this.password)
      .subscribe(user => {
        this.log.emit(true);
        localStorage.setItem('username', this.username);
        localStorage.setItem('password', this.password);
      }, () => {
        const message = 'Invalid username or passwod';
        alert(message);
      });
  }
}
