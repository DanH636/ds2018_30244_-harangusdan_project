import {Component, OnInit} from '@angular/core';
import {Car} from '../models/Car';
import {AddCarService} from '../services/add-car/add-car.service';
import {LoginService} from '../services/login/login.service';
import {FavoritesService} from '../services/favorites/favorites.service';
import {FavoriteCar} from '../models/FavoriteCar';
import {User} from '../models/User';
import {CommentService} from '../services/comment/comment.service';
import {Comments} from '../models/Comments';
import {DealsService} from '../services/deals/deals.service';
import {CarDeal} from '../models/CarDeal';

@Component({
  selector: 'app-car-list',
  templateUrl: './car-list.component.html',
  styleUrls: ['./car-list.component.css']
})
export class CarListComponent implements OnInit {

  carsList: Car[];
  user: User;
  comments: Comments[];
  userCarDeals: CarDeal[];

  constructor(private carService: AddCarService,
              private loginService: LoginService,
              private favoriteService: FavoritesService,
              private commentService: CommentService,
              private dealsService: DealsService) {
  }

  ngOnInit() {
    this.carService.getCars().subscribe(cars => this.carsList = cars);
    this.commentService.getAllComments().subscribe(comments => this.comments = comments);
    this.dealsService.getAllCarDeals(localStorage.getItem('username')).subscribe(userDeals => this.userCarDeals = userDeals);
  }

  addCarToFavorite(car: Car) {
    this.loginService.getUser(localStorage.getItem('username'), localStorage.getItem('password'))
      .subscribe(user => {
        this.user = user;
        const favCar = new FavoriteCar();
        favCar.car = car;
        favCar.user = user;
        this.favoriteService.putFavoriteCar(favCar).subscribe();
      });
  }

  addCarToLocalStorage(car: Car) {
    localStorage.setItem('dealCar', car.model);
  }

  deleteCar(car: Car) {
    this.loginService.getUser(localStorage.getItem('username'), localStorage.getItem('password'))
      .subscribe(user => {
        console.log(user);
        if (user.authority === 'Administrator') {
          this.carService.deleteCar(car.model).subscribe();
        } else {
          const message = 'You do not have admin authority';
          alert(message);
        }
      });
  }
}
