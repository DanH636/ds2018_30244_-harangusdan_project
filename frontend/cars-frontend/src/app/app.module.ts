import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { AddPageComponent } from './add-page/add-page.component';
import { LoginComponent } from './login/login.component';
import { MainPageComponent } from './main-page/main-page.component';
import { CarListComponent } from './car-list/car-list.component';
import { PartListComponent } from './part-list/part-list.component';
import { FavoritesComponent } from './favorites/favorites.component';
import { DealsComponent } from './deals/deals.component';
import { PartDealComponent } from './part-deal/part-deal.component';
import { CommentsComponent } from './comments/comments.component';
import { PriceHistoryComponent } from './price-history/price-history.component';

@NgModule({
  declarations: [
    AppComponent,
    AddPageComponent,
    LoginComponent,
    MainPageComponent,
    CarListComponent,
    PartListComponent,
    FavoritesComponent,
    DealsComponent,
    PartDealComponent,
    CommentsComponent,
    PriceHistoryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
