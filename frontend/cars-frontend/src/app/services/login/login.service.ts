import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../../models/User';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  endPoint = 'http://localhost:8080/users/';

  constructor(private http: HttpClient) {
  }

  getUser(username: string, password: string): Observable<User> {
    const userEndpoint = this.endPoint + username + '/' + password;
    return this.http.get<User>(userEndpoint);
  }
}
