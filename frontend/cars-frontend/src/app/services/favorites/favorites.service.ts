import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {FavoriteCar} from '../../models/FavoriteCar';
import {Observable} from 'rxjs';
import {FavoritePart} from '../../models/FavoritePart';

@Injectable({
  providedIn: 'root'
})
export class FavoritesService {

  carEndPoint = 'http://localhost:8080/favorites/';
  partEndPoint = 'http://localhost:8080/favoritesParts/';

  constructor(private  http: HttpClient) {
  }

  putFavoriteCar(favoriteCar: FavoriteCar): Observable<void> {
    const putFavCarEndpoint = this.carEndPoint + 'fav';
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa(localStorage.getItem('username') + ':' + localStorage.getItem('password')));
    return this.http.put<void>(putFavCarEndpoint, favoriteCar, {headers: headers});

  }

  getUserFavCars(username: string): Observable<FavoriteCar[]> {
    const getUsersFavCarsEndpoint = this.carEndPoint + username;
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa(localStorage.getItem('username') + ':' + localStorage.getItem('password')));
    return this.http.get<FavoriteCar[]>(getUsersFavCarsEndpoint, {headers: headers});
  }

  putFavoritePart(favoritePart: FavoritePart): Observable<void> {
    const putFavPartEndpoint = this.partEndPoint + 'fav';
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa(localStorage.getItem('username') + ':' + localStorage.getItem('password')));
    return this.http.put<void>(putFavPartEndpoint, favoritePart, {headers: headers});
  }

  getUserFavParts(username: string): Observable<FavoritePart[]> {
    const getFavPartsEndpoint = this.partEndPoint + username;
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa(localStorage.getItem('username') + ':' + localStorage.getItem('password')));
    return this.http.get<FavoritePart[]>(getFavPartsEndpoint, {headers: headers});
  }

  deleteFavPart(part: string): Observable<void> {
    const deleteFavPartEndpoint = this.partEndPoint + part;
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa(localStorage.getItem('username') + ':' + localStorage.getItem('password')));
    return this.http.delete<void>(deleteFavPartEndpoint, {headers: headers});
  }

  deleteFavCar(model: string): Observable<void> {
    const deleteFavCarEndpoint = this.carEndPoint + model;
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa(localStorage.getItem('username') + ':' + localStorage.getItem('password')));
    return this.http.delete<void>(deleteFavCarEndpoint, {headers: headers});
  }
}
