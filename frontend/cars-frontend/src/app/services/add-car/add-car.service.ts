import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Car} from '../../models/Car';
import {Observable} from 'rxjs';
import {CarPriceHistory} from '../../models/CarPriceHistory';

@Injectable({
  providedIn: 'root'
})
export class AddCarService {

  endPoint = 'http://localhost:8080/cars/';
  priceHistoryEndPoint = 'http://localhost:8080/prices/';


  constructor(private http: HttpClient) {
  }

  putCar(car: Car): Observable<void> {
    const putCarEndpoint = this.endPoint + 'car';
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa(localStorage.getItem('username') + ':' + localStorage.getItem('password')));
    return this.http.put<void>(putCarEndpoint, car, {headers: headers});
  }

  getCar(model: string): Observable<Car> {
    const getCarEndpoint = this.endPoint + model;
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa(localStorage.getItem('username') + ':' + localStorage.getItem('password')));
    return this.http.get<Car>(getCarEndpoint, {headers: headers});
  }

  getCars(): Observable<Car[]> {
    const getCarsEndpoint = this.endPoint + 'all';
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa(localStorage.getItem('username') + ':' + localStorage.getItem('password')));
    return this.http.get<Car[]>(getCarsEndpoint, {headers: headers});
  }

  deleteCar(model: string): Observable<void> {
    const deleteCarEndpoint = this.endPoint + model;
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa(localStorage.getItem('username') + ':' + localStorage.getItem('password')));
    return this.http.delete<void>(deleteCarEndpoint, {headers: headers});
  }

  getCarPriceHistory(model: string): Observable<CarPriceHistory[]> {
    const getCarPriceHistoryEndPoint = this.priceHistoryEndPoint + model;
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa(localStorage.getItem('username') + ':' + localStorage.getItem('password')));
    return this.http.get<CarPriceHistory[]>(getCarPriceHistoryEndPoint, {headers: headers});
  }
}
