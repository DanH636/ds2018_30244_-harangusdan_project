import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {CarDeal} from '../../models/CarDeal';
import {Observable} from 'rxjs';
import {PartDeal} from '../../models/PartDeal';

@Injectable({
  providedIn: 'root'
})
export class DealsService {

  carDealEndpoint = 'http://localhost:8080/carDeals/';
  partDealEndpoint = 'http://localhost:8080/partDeals/';

  constructor(private http: HttpClient) {
  }

  putCarDeal(carDeal: CarDeal): Observable<void> {
    const putCarDealEndpoint = this.carDealEndpoint + 'car';
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa(localStorage.getItem('username') + ':' + localStorage.getItem('password')));
    return this.http.put<void>(putCarDealEndpoint, carDeal, {headers: headers});
  }

  putPartDeal(partDeal: PartDeal): Observable<void> {
    const putPartDealEndpoint = this.partDealEndpoint + 'part';
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa(localStorage.getItem('username') + ':' + localStorage.getItem('password')));
    return this.http.put<void>(putPartDealEndpoint, partDeal, {headers: headers});
  }

  getAllCarDeals(username: string): Observable<CarDeal[]> {
    const getAllCarDeals = this.carDealEndpoint + username;
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa(localStorage.getItem('username') + ':' + localStorage.getItem('password')));
    return this.http.get<CarDeal[]>(getAllCarDeals, {headers: headers});
  }

  getAllPartDeals(username: string): Observable<PartDeal[]> {
    const getAllPartDeals = this.partDealEndpoint + username;
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa(localStorage.getItem('username') + ':' + localStorage.getItem('password')));
    return this.http.get<PartDeal[]>(getAllPartDeals, {headers: headers});
  }
}
