import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Comments} from '../../models/Comments';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  commentEndpoint = 'http://localhost:8080/comments/';

  constructor(private http: HttpClient) {
  }

  putComment(comment: Comments): Observable<void> {
    const putCommentEndpoint = this.commentEndpoint + 'comment';
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa(localStorage.getItem('username') + ':' + localStorage.getItem('password')));
    return this.http.put<void>(putCommentEndpoint, comment, {headers: headers});
  }

  getAllComments(): Observable<Comments[]> {
    const getAllCommentsEndpoint = this.commentEndpoint + 'all';
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa(localStorage.getItem('username') + ':' + localStorage.getItem('password')));
    return this.http.get<Comments[]>(getAllCommentsEndpoint, {headers: headers});
  }
}

