import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Part} from '../../models/Part';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PartService {

  endPoint = 'http://localhost:8080/parts/';

  constructor(private http: HttpClient) {
  }

  putPart(part: Part): Observable<void> {
    const putPartEndpoint = this.endPoint + 'part';
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa(localStorage.getItem('username') + ':' + localStorage.getItem('password')));
    return this.http.put<void>(putPartEndpoint, part, {headers: headers});
  }

  getPart(part: string): Observable<Part> {
    const getPartEndpoint = this.endPoint + part;
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa(localStorage.getItem('username') + ':' + localStorage.getItem('password')));
    return this.http.get<Part>(getPartEndpoint, {headers: headers});
  }

  deletePart(part: string): Observable<void> {
    const deletePartEndpoint = this.endPoint + part;
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa(localStorage.getItem('username') + ':' + localStorage.getItem('password')));
    return this.http.delete<void>(deletePartEndpoint, {headers: headers});
  }

  getAllParts(): Observable<Part[]> {
    const getAllPartsEndpoint = this.endPoint + 'all';
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa(localStorage.getItem('username') + ':' + localStorage.getItem('password')));
    return this.http.get<Part[]>(getAllPartsEndpoint, {headers: headers});
  }
}
