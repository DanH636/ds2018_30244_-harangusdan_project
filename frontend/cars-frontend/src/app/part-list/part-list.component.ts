import {Component, OnInit} from '@angular/core';
import {Part} from '../models/Part';
import {PartService} from '../services/part/part.service';
import {LoginService} from '../services/login/login.service';
import {FavoritePart} from '../models/FavoritePart';
import {FavoritesService} from '../services/favorites/favorites.service';
import {User} from '../models/User';
import {DealsService} from '../services/deals/deals.service';
import {PartDeal} from '../models/PartDeal';

@Component({
  selector: 'app-part-list',
  templateUrl: './part-list.component.html',
  styleUrls: ['./part-list.component.css']
})
export class PartListComponent implements OnInit {

  partList: Part[];
  user: User;
  userPartDeals: PartDeal[];

  constructor(private partService: PartService,
              private loginService: LoginService,
              private favoriteService: FavoritesService,
              private dealsService: DealsService) {
  }

  ngOnInit() {
    this.partService.getAllParts().subscribe(parts => this.partList = parts);
    this.dealsService.getAllPartDeals(localStorage.getItem('username')).subscribe( userDeals => this.userPartDeals = userDeals);
  }

  addPartToFavorite(part: Part) {
    this.loginService.getUser(localStorage.getItem('username'), localStorage.getItem('password'))
      .subscribe(user => {
        this.user = user;
        const favPart = new FavoritePart();
        favPart.part = part;
        favPart.user = user;
        this.favoriteService.putFavoritePart(favPart).subscribe();
      });
  }

  addPartToStorage(part: Part) {
    localStorage.setItem('dealPart', part.part);
  }

  deletePart(part: Part) {
    this.loginService.getUser(localStorage.getItem('username'), localStorage.getItem('password'))
      .subscribe(user => {
        if (user.authority === 'Administrator') {
          this.partService.deletePart(part.part).subscribe();
        } else {
          const message = 'You do not have admin authority';
          alert(message);
        }
      });
  }
}
