import {Component, OnInit} from '@angular/core';
import {FavoritesService} from '../services/favorites/favorites.service';
import {FavoriteCar} from '../models/FavoriteCar';
import {FavoritePart} from '../models/FavoritePart';
import {Car} from '../models/Car';
import {Part} from '../models/Part';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.css']
})
export class FavoritesComponent implements OnInit {

  favCarsList: FavoriteCar[];
  favPartsList: FavoritePart[];

  constructor(private favoriteService: FavoritesService) {
  }

  ngOnInit() {
    this.favoriteService.getUserFavParts(localStorage.getItem('username')).subscribe(favParts => this.favPartsList = favParts);
    this.favoriteService.getUserFavCars(localStorage.getItem('username')).subscribe(favCars => this.favCarsList = favCars);
  }


  deleteFavCar(favoriteCar: FavoriteCar) {
    this.favoriteService.deleteFavCar(favoriteCar.car.model).subscribe();
  }

  deleteFavPart(favoritePart: FavoritePart) {
    this.favoriteService.deleteFavPart(favoritePart.part.part).subscribe();
  }
}
