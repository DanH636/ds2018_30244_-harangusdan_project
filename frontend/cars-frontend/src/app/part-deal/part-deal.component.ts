import {Component, OnInit} from '@angular/core';
import {User} from '../models/User';
import {Car} from '../models/Car';
import {DealsService} from '../services/deals/deals.service';
import {LoginService} from '../services/login/login.service';
import {AddCarService} from '../services/add-car/add-car.service';
import {PartDeal} from '../models/PartDeal';
import {Part} from '../models/Part';
import {PartService} from '../services/part/part.service';

@Component({
  selector: 'app-part-deal',
  templateUrl: './part-deal.component.html',
  styleUrls: ['./part-deal.component.css']
})
export class PartDealComponent implements OnInit {

  user: User;
  part: Part;
  offerAmount: number;

  constructor(private dealsService: DealsService,
              private loginService: LoginService,
              private partService: PartService) {
  }

  ngOnInit() {
    this.loginService.getUser(localStorage.getItem('username'), localStorage.getItem('password'))
      .subscribe(user => {
        this.user = user;
        this.partService.getPart(localStorage.getItem('dealPart')).subscribe(part => {
          this.part = part;
        });
      });
  }

  addPartDeal() {
    const partDeal = new PartDeal();
    partDeal.ownerUser = this.part.user;
    partDeal.dealUser = this.user;
    partDeal.partId = this.part;
    partDeal.dealAmount = this.offerAmount;
    this.dealsService.putPartDeal(partDeal).subscribe();
  }
}
