import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartDealComponent } from './part-deal.component';

describe('PartDealComponent', () => {
  let component: PartDealComponent;
  let fixture: ComponentFixture<PartDealComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartDealComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartDealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
