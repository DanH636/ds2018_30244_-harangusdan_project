import {Component, OnInit} from '@angular/core';
import {Car} from '../models/Car';
import {LoginService} from '../services/login/login.service';
import {AddCarService} from '../services/add-car/add-car.service';
import {PartService} from '../services/part/part.service';
import {Part} from '../models/Part';

@Component({
  selector: 'app-add-page',
  templateUrl: './add-page.component.html',
  styleUrls: ['./add-page.component.css']
})
export class AddPageComponent implements OnInit {
  model: string;
  year: number;
  price: number;
  description: string;
  part: string;
  partPrice: number;
  carModel: string;
  partDescription: string;

  constructor(private loginService: LoginService,
              private carService: AddCarService,
              private partService: PartService) {
  }

  ngOnInit() {
    console.log(localStorage.getItem('username'));
    console.log(localStorage.getItem('password'));
  }

  addCar() {
    this.loginService.getUser(localStorage.getItem('username'), localStorage.getItem('password'))
      .subscribe(user => {
        console.log(user);
        const car = new Car();
        car.model = this.model;
        car.year = this.year;
        car.price = this.price;
        car.description = this.description;
        car.user = user;
        car.favorite = 0;
        console.log(car);
        this.carService.putCar(car).subscribe();
      });

  }

  addPart() {
    this.loginService.getUser(localStorage.getItem('username'), localStorage.getItem('password'))
      .subscribe(user => {
        this.carService.getCar(this.carModel).subscribe(car => {
          const part = new Part();
          part.part = this.part;
          part.price = this.partPrice;
          part.description = this.partDescription;
          part.car = car;
          part.user = user;
          part.favorite = 0;
          console.log(part);
          this.partService.putPart(part).subscribe();
        });
      });
  }
}
