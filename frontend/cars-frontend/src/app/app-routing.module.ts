import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AddPageComponent} from './add-page/add-page.component';
import {LoginComponent} from './login/login.component';
import {CarListComponent} from './car-list/car-list.component';
import {PartListComponent} from './part-list/part-list.component';
import {FavoritesComponent} from './favorites/favorites.component';
import {DealsComponent} from './deals/deals.component';
import {PartDealComponent} from './part-deal/part-deal.component';
import {CommentsComponent} from './comments/comments.component';
import {PriceHistoryComponent} from './price-history/price-history.component';

const routes: Routes = [
  {path: 'add-page', component: AddPageComponent},
  {path: 'login', component: LoginComponent},
  {path: 'all-cars', component: CarListComponent},
  {path: 'all-parts', component: PartListComponent},
  {path: 'favorites', component: FavoritesComponent},
  {path: 'deals', component: DealsComponent},
  {path: 'part-deal', component: PartDealComponent},
  {path: 'comments', component: CommentsComponent},
  {path: 'history', component: PriceHistoryComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
