import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Chart} from 'chart.js';
import {AddCarService} from '../services/add-car/add-car.service';
import {CarPriceHistory} from '../models/CarPriceHistory';

@Component({
  selector: 'app-price-history',
  templateUrl: './price-history.component.html',
  styleUrls: ['./price-history.component.css']
})
export class PriceHistoryComponent implements OnInit, AfterViewInit {

  @ViewChild('myChart') myChart;

  chartdata: CarPriceHistory[];
  chart: any;

  constructor(private carService: AddCarService) {
  }

  ngOnInit() {
  }

  initCanvas() {
    this.carService.getCarPriceHistory(localStorage.getItem('dealCar')).subscribe(prices => {
      this.chartdata = prices;
      const chartValues = [];
      for (const chart of this.chartdata) {
        chartValues.push(chart.price);
      }
      console.log(chartValues);
      const canvas: any = document.getElementById('myChart');
      this.chart = new Chart(this.myChart.nativeElement.getContext('2d'), {
        type: 'line',
        data: {
          labels: chartValues,
          datasets: [
            {
              data: chartValues,
              borderColor: '#3cba9f',
              fill: false
            }
          ]
        },
        options: {
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              display: true
            }],
            yAxes: [{
              display: true
            }],
          }
        }
      });
    });
  }

  ngAfterViewInit(): void {
    this.initCanvas();
  }
}
