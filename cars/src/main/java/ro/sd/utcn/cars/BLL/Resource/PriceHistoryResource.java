package ro.sd.utcn.cars.BLL.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.sd.utcn.cars.BLL.PriceHistoryService;
import ro.sd.utcn.cars.Models.Car;
import ro.sd.utcn.cars.Models.CarPriceHistory;

import java.util.List;

@RestController
@RequestMapping(value = "/prices/")
public class PriceHistoryResource {

    @Autowired
    PriceHistoryService priceHistoryService;

    @GetMapping(value = "/{model}", produces = "application/json")
    public List<CarPriceHistory> getPricesForCar(@PathVariable("model") String model) {
        return priceHistoryService.findAllByCarModel(model);
    }

    @PutMapping(value = "/price", produces = "application/json")
    public void putPriceForCar(@RequestBody CarPriceHistory cph) {
        priceHistoryService.savePriceForCar(cph);
    }
}
