package ro.sd.utcn.cars.Models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "price_history")
public class CarPriceHistory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne(targetEntity = Car.class)
    @JoinColumn(name="car_id")
    private Car carId;

    @Column
    private int price;

    public CarPriceHistory() {
    }

    public CarPriceHistory(Car carId, int price) {
        this.carId = carId;
        this.price = price;
    }

    public Car getCarId() {
        return carId;
    }

    public void setCarId(Car carId) {
        this.carId = carId;
    }


    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "CarPriceHistory{" +
                "carId=" + carId +
                ", price=" + price +
                '}';
    }
}
