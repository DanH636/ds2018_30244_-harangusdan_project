package ro.sd.utcn.cars.Models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "part_deals")
public class PartDeal implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name="owner_user")
    private User ownerUser;

    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name="deal_user")
    private User dealUser;

    @Column(name = "deal_amount")
    private int dealAmount;

    @ManyToOne(targetEntity = Part.class)
    @JoinColumn(name="part_id")
    private Part partId;

    public PartDeal() {
    }

    public PartDeal(User ownerUser, User dealUser, int dealAmount, Part partId) {
        this.ownerUser = ownerUser;
        this.dealUser = dealUser;
        this.dealAmount = dealAmount;
        this.partId = partId;
    }

    public User getOwnerUser() {
        return ownerUser;
    }

    public void setOwnerUser(User ownerUser) {
        this.ownerUser = ownerUser;
    }

    public User getDealUser() {
        return dealUser;
    }

    public void setDealUser(User dealUser) {
        this.dealUser = dealUser;
    }

    public int getDealAmount() {
        return dealAmount;
    }

    public void setDealAmount(int dealAmount) {
        this.dealAmount = dealAmount;
    }

    public Part getPartId() {
        return partId;
    }

    public void setPartId(Part partId) {
        this.partId = partId;
    }

    @Override
    public String toString() {
        return "PartDeal{" +
                "ownerUser=" + ownerUser +
                ", dealUser=" + dealUser +
                ", dealAmount=" + dealAmount +
                ", partId=" + partId +
                '}';
    }
}
