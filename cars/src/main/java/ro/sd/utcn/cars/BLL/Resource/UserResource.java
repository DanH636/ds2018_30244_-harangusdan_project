package ro.sd.utcn.cars.BLL.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import ro.sd.utcn.cars.BLL.UserService;
import ro.sd.utcn.cars.Models.User;

@RestController
@RequestMapping(value = "/users")
public class UserResource {

    @Autowired
    UserService userService;

    @GetMapping(value = "/{username}/{password}", produces = "application/json")
    public User getUser(@PathVariable("username") String username, @PathVariable("password") String password) {
        return userService.findUser(username, password);
    }

    @PutMapping(value = "/user", produces = "application/json")
    public void putUser(@RequestBody User user) {
        PasswordEncoder encoder = new BCryptPasswordEncoder(10);
        user.setPassword(encoder.encode(user.getPassword()));
        userService.saveUser(user);
    }

    @DeleteMapping(value = "/{username}/{password}", produces = "application/json")
    public void deleteUser(@PathVariable("username") String username, @PathVariable("password") String password) {
        User user = userService.findUser(username, password);
        userService.deleteUser(user);
    }
}
