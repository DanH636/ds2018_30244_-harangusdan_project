package ro.sd.utcn.cars.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.sd.utcn.cars.Models.FavoritePart;
import ro.sd.utcn.cars.Models.User;

import java.util.List;

public interface FavoritePartDAO extends JpaRepository<FavoritePart, Integer> {

    public List<FavoritePart> findAllByUser_Username(String username);

    public FavoritePart findByPart_Part(String part);
}
