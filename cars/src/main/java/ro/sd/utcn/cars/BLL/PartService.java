package ro.sd.utcn.cars.BLL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sd.utcn.cars.DAO.PartDAO;
import ro.sd.utcn.cars.Models.Part;

import java.util.List;

@Service
public class PartService {

    @Autowired
    PartDAO partDAO;

    public Part findPart(String part) {
        return partDAO.findByPart(part);
    }

    public List<Part> findAllParts() {
        return  partDAO.findAll();
    }

    public void savePart(Part part){
        partDAO.save(part);
    }

    public void deletePart(Part part){
        partDAO.delete(part);
    }
}
