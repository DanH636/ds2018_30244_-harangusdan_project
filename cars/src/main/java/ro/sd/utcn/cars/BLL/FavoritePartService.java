package ro.sd.utcn.cars.BLL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sd.utcn.cars.DAO.FavoritePartDAO;
import ro.sd.utcn.cars.Models.FavoritePart;
import ro.sd.utcn.cars.Models.User;

import java.util.List;

@Service
public class FavoritePartService {

    @Autowired
    FavoritePartDAO favoritePartDAO;

    public List<FavoritePart> findAllByUser(String username){
        return favoritePartDAO.findAllByUser_Username(username);
    }

    public FavoritePart findByPart(String part){
        return favoritePartDAO.findByPart_Part(part);
    }

    public void addFavoritePart(FavoritePart favoritePart){
        favoritePartDAO.save(favoritePart);
    }

    public void deleteFavoritePart(FavoritePart favoritePart) {
        favoritePartDAO.delete(favoritePart);
    }
}
