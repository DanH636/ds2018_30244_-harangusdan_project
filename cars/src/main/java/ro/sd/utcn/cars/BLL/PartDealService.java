package ro.sd.utcn.cars.BLL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sd.utcn.cars.DAO.PartDealDAO;
import ro.sd.utcn.cars.Models.Part;
import ro.sd.utcn.cars.Models.PartDeal;
import ro.sd.utcn.cars.Models.User;

import java.util.List;

@Service
public class PartDealService {

    @Autowired
    PartDealDAO partDealDAO;

    public List<PartDeal> findAllByOwner(String username){
        return partDealDAO.findAllByOwnerUser_Username(username);
    }

    public List<PartDeal> findAllByPart(Part part){
        return partDealDAO.findAllByPartId(part);
    }

    public void deletePartDeal(PartDeal partDeal){
        partDealDAO.delete(partDeal);
    }

    public void savePartDeal(PartDeal partDeal){
        partDealDAO.save(partDeal);
    }
}
