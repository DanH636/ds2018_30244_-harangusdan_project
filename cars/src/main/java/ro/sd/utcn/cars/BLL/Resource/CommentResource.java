package ro.sd.utcn.cars.BLL.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.sd.utcn.cars.BLL.CommentService;
import ro.sd.utcn.cars.Models.Car;
import ro.sd.utcn.cars.Models.Comment;
import ro.sd.utcn.cars.Models.User;

import java.util.List;

@RestController
@RequestMapping(value = "/comments")
public class CommentResource {

    @Autowired
    CommentService commentService;

    @GetMapping(value = "/{carId}", produces = "application/json")
    public List<Comment> getCommentsByCar(@PathVariable("carId") Car carId) {
        return commentService.findByCarId(carId);
    }

    @GetMapping(value = "/{userId}", produces = "application/json")
    public List<Comment> getCommentsByUser(@PathVariable("userId") User userId) {
        return commentService.findByUserId(userId);
    }

    @GetMapping(value = "/all", produces = "application/json")
    public List<Comment> getAllComments(){
        return commentService.findAllComments();
    }

    @PutMapping(value = "/comment", produces = "application/json")
    public void putComment(@RequestBody Comment comment) {
        commentService.saveComment(comment);
    }

    @DeleteMapping(value = "/{id}", produces = "application/json")
    public void deleteComment(@PathVariable("id") int id) {
        Comment comment = commentService.findById(id);
        commentService.deleteComment(comment);
    }
}
