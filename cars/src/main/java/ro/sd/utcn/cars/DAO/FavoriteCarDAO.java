package ro.sd.utcn.cars.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.sd.utcn.cars.Models.FavoriteCar;
import ro.sd.utcn.cars.Models.User;

import java.util.List;

public interface FavoriteCarDAO extends JpaRepository<FavoriteCar, Integer> {

    public List<FavoriteCar> findAllByUser_Username(String username);

    public FavoriteCar findByCar_Model(String model);

}
