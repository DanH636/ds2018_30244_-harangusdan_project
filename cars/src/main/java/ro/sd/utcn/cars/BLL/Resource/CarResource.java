package ro.sd.utcn.cars.BLL.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.sd.utcn.cars.BLL.CarDealService;
import ro.sd.utcn.cars.BLL.CarService;
import ro.sd.utcn.cars.BLL.CommentService;
import ro.sd.utcn.cars.Models.Car;
import ro.sd.utcn.cars.Models.CarDeal;
import ro.sd.utcn.cars.Models.Comment;

import java.util.List;

@RestController
@RequestMapping(value = "/cars")
public class CarResource {

    @Autowired
    CarService carService;

    @Autowired
    CarDealService carDealService;

    @Autowired
    CommentService commentService;

    @GetMapping(value = "/{model}", produces = "application/json")
    public Car getCar(@PathVariable("model") String model) {
        return carService.findCar(model);
    }

    @PutMapping(value = "/car", produces = "application/json")
    public void putCar(@RequestBody Car car) {
        carService.saveCar(car);
    }

    @DeleteMapping(value = "/{model}", produces = "application/json")
    public void deleteCar(@PathVariable("model") String model) {
        Car car = carService.findCar(model);
        carService.deleteCar(car);
        List<CarDeal> carDeals = carDealService.findAllByCar(car);
        for(CarDeal carDeal : carDeals){
            carDealService.deleteCarDeal(carDeal);
        }
        List<Comment> comments = commentService.findByCarId(car);
        for(Comment comment: comments){
            commentService.deleteComment(comment);
        }
    }

    @GetMapping(value = "/all", produces = "application/json")
    public List<Car> getAllCars() {
        return carService.findAllCars();
    }

}
