package ro.sd.utcn.cars.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.sd.utcn.cars.Models.Part;
import ro.sd.utcn.cars.Models.PartDeal;
import ro.sd.utcn.cars.Models.User;

import java.util.List;

public interface PartDealDAO extends JpaRepository<PartDeal, Integer> {

    public List<PartDeal> findAllByOwnerUser_Username(String username);

    public List<PartDeal> findAllByPartId(Part part);
}
