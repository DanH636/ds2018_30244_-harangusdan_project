package ro.sd.utcn.cars.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.sd.utcn.cars.Models.User;

@Repository
public interface UserDAO extends JpaRepository<User, Integer> {

    public User findByUsernameAndPassword(String username, String password);
}
