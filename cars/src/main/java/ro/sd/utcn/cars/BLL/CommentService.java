package ro.sd.utcn.cars.BLL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sd.utcn.cars.DAO.CommentsDAO;
import ro.sd.utcn.cars.Models.Car;
import ro.sd.utcn.cars.Models.Comment;
import ro.sd.utcn.cars.Models.User;

import java.util.List;

@Service
public class CommentService {

    @Autowired
    CommentsDAO commentsDAO;

    public List<Comment> findByCarId(Car carId){
        return commentsDAO.findAllByCarId(carId);
    }

    public List<Comment> findByUserId(User userId){
        return commentsDAO.findAllByUserId(userId);
    }

    public List<Comment> findAllComments(){
        return commentsDAO.findAll();
    }

    public void saveComment(Comment comment){
        commentsDAO.save(comment);
    }

    public Comment findById(int id){
        return commentsDAO.findById(id);
    }

    public void deleteComment(Comment comment){
        commentsDAO.delete(comment);
    }
}
