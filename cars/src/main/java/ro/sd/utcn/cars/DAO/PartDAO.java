package ro.sd.utcn.cars.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.sd.utcn.cars.Models.Part;

import java.util.List;

public interface PartDAO extends JpaRepository<Part, Integer> {

    public Part findByPart(String part);
}
