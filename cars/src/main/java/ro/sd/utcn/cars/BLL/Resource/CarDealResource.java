package ro.sd.utcn.cars.BLL.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.sd.utcn.cars.BLL.CarDealService;
import ro.sd.utcn.cars.BLL.Services.MailService;
import ro.sd.utcn.cars.Models.CarDeal;
import ro.sd.utcn.cars.Models.User;

import java.util.List;

@RestController
@RequestMapping(value = "/carDeals")
public class CarDealResource {

    @Autowired
    CarDealService carDealService;


    @GetMapping(value = "/{username}", produces = "application/json")
    public List<CarDeal> getAllCarDeals(@PathVariable("username") String username) {
        return carDealService.findAllByOwner(username);
    }

    @PutMapping(value = "/car", produces = "application/json")
    public void putCarDeal(@RequestBody CarDeal carDeal) {
        final MailService mailService = new MailService("danuharangus@gmail.com ", "Ibanez6396");
        String message = "You receive a deal for " + carDeal.getCarId().getModel() + " from " + carDeal.getDealUser().getUsername();
        mailService.sendMail("danuharangus@gmail.com ", "danuharangus@gmail.com ", message);
        carDealService.saveCarDeal(carDeal);
    }
}
