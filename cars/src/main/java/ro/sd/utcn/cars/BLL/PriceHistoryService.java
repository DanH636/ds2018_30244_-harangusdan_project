package ro.sd.utcn.cars.BLL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sd.utcn.cars.DAO.PriceHistoryDAO;
import ro.sd.utcn.cars.Models.Car;
import ro.sd.utcn.cars.Models.CarPriceHistory;

import java.util.List;

@Service
public class PriceHistoryService {

    @Autowired
    PriceHistoryDAO priceHistoryDAO;

    public List<CarPriceHistory> findAllByCarModel(String model){
        return priceHistoryDAO.findAllByCarId_Model(model);
    }

    public void savePriceForCar(CarPriceHistory carPriceHistory){
        priceHistoryDAO.save(carPriceHistory);
    }
}
