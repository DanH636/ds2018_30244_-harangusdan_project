package ro.sd.utcn.cars.BLL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sd.utcn.cars.BLL.Services.MailService;
import ro.sd.utcn.cars.DAO.CarDealDAO;
import ro.sd.utcn.cars.Models.Car;
import ro.sd.utcn.cars.Models.CarDeal;
import ro.sd.utcn.cars.Models.User;

import java.util.List;

@Service
public class CarDealService {

    @Autowired
    CarDealDAO carDealDAO;

    public List<CarDeal> findAllByOwner(String username){
        return carDealDAO.findAllByOwnerUser_Username(username);
    }

    public List<CarDeal> findAllByCar(Car car){
        return carDealDAO.findAllByCarId(car);
    }

    public void saveCarDeal(CarDeal carDeal){
        carDealDAO.save(carDeal);
    }

    public void deleteCarDeal(CarDeal carDeal) {
        carDealDAO.delete(carDeal);
    }
}
