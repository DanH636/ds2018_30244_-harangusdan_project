package ro.sd.utcn.cars.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.sd.utcn.cars.Models.Car;
import ro.sd.utcn.cars.Models.Comment;
import ro.sd.utcn.cars.Models.User;

import java.util.List;

public interface CommentsDAO extends JpaRepository<Comment, Integer>{

    public List<Comment> findAllByUserId(User userId);

    public List<Comment> findAllByCarId(Car carId);

    public Comment findById(int id);
}
