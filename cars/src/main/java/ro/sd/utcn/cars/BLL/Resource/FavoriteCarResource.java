package ro.sd.utcn.cars.BLL.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.sd.utcn.cars.BLL.FavoriteCarService;
import ro.sd.utcn.cars.Models.FavoriteCar;
import ro.sd.utcn.cars.Models.User;

import java.util.List;

@RestController
@RequestMapping(value = "/favorites")
public class FavoriteCarResource {

    @Autowired
    FavoriteCarService favoriteCarService;

    @GetMapping(value="/{username}", produces = "application/json")
    public List<FavoriteCar> getAllFavoriteCars(@PathVariable("username") String username){
        return favoriteCarService.findAllByUser(username);
    }

    @DeleteMapping(value = "/{model}", produces = "application/json")
    public void deleteFavoriteCar(@PathVariable("model") String model){
        FavoriteCar favoriteCar = favoriteCarService.findByModel(model);
        favoriteCarService.deleteFavoriteCar(favoriteCar);
    }

    @PutMapping(value = "/fav", produces = "application/json")
    public void putFavoriteCar(@RequestBody FavoriteCar favoriteCar) {
        favoriteCarService.saveFavoriteCar(favoriteCar);
    }
}
