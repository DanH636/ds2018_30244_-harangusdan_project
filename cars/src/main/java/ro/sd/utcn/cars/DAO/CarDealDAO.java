package ro.sd.utcn.cars.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.sd.utcn.cars.Models.Car;
import ro.sd.utcn.cars.Models.CarDeal;
import ro.sd.utcn.cars.Models.User;

import java.util.List;

public interface CarDealDAO extends JpaRepository<CarDeal, Integer> {

    public List<CarDeal> findAllByOwnerUser_Username (String username);

    public List<CarDeal> findAllByCarId (Car carId);
}
