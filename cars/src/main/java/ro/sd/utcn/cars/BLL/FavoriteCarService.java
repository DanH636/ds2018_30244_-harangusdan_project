package ro.sd.utcn.cars.BLL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sd.utcn.cars.DAO.FavoriteCarDAO;
import ro.sd.utcn.cars.Models.FavoriteCar;
import ro.sd.utcn.cars.Models.User;

import java.util.List;

@Service
public class FavoriteCarService {

    @Autowired
    FavoriteCarDAO favoriteCarDAO;

    public List<FavoriteCar> findAllByUser(String username){
        return favoriteCarDAO.findAllByUser_Username(username);
    }

    public FavoriteCar findByModel(String model) {
        return favoriteCarDAO.findByCar_Model(model);
    }

    public void saveFavoriteCar(FavoriteCar favoriteCar){
        favoriteCarDAO.save(favoriteCar);
    }

    public void deleteFavoriteCar(FavoriteCar favoriteCar) {
        favoriteCarDAO.delete(favoriteCar);
    }


}
