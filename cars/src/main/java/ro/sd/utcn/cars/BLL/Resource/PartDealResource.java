package ro.sd.utcn.cars.BLL.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.sd.utcn.cars.BLL.PartDealService;
import ro.sd.utcn.cars.BLL.Services.MailService;
import ro.sd.utcn.cars.Models.PartDeal;
import ro.sd.utcn.cars.Models.User;

import java.util.List;

@RestController
@RequestMapping(value = "/partDeals")
public class PartDealResource {

    @Autowired
    PartDealService partDealService;

    @GetMapping(value = "/{username}", produces = "application/json")
    public List<PartDeal> getAllPartDeals(@PathVariable("username") String username) {
        return partDealService.findAllByOwner(username);
    }

    @PutMapping(value = "/part", produces = "application/json")
    public void putPairDeal(@RequestBody PartDeal partDeal) {
        final MailService mailService = new MailService("danuharangus@gmail.com ", "Ibanez6396");
        String message = "You receive a deal for " + partDeal.getPartId().getPart() + " from " + partDeal.getDealUser().getUsername();
        mailService.sendMail("danuharangus@gmail.com ", "danuharangus@gmail.com ", message);
        partDealService.savePartDeal(partDeal);
    }
}
