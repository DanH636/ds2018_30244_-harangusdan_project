package ro.sd.utcn.cars.BLL.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.sd.utcn.cars.BLL.PartDealService;
import ro.sd.utcn.cars.BLL.PartService;
import ro.sd.utcn.cars.Models.Part;
import ro.sd.utcn.cars.Models.PartDeal;

import java.util.List;

@RestController
@RequestMapping(value = "/parts")
public class PartResource {

    @Autowired
    PartService partService;

    @Autowired
    PartDealService partDealService;

    @GetMapping(value = "/{part}", produces = "application/json")
    public Part getPart(@PathVariable("part") String part) {
        return partService.findPart(part);
    }

    @GetMapping(value = "/all", produces = "application/json")
    public List<Part> getAllParts(){
        return partService.findAllParts();
    }


    @PutMapping(value = "/part", produces = "application/json")
    public void putPart(@RequestBody Part part) {
        partService.savePart(part);
    }

    @DeleteMapping(value = "/{part}", produces = "application/json")
    public void deletePart(@PathVariable("part") String part) {
        Part findPart = partService.findPart(part);
        partService.deletePart(findPart);

        List<PartDeal> partDeals = partDealService.findAllByPart(findPart);
        for(PartDeal partDeal: partDeals){
            partDealService.deletePartDeal(partDeal);
        }
    }
}
