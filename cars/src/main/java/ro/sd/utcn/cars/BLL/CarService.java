package ro.sd.utcn.cars.BLL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sd.utcn.cars.DAO.CarDAO;
import ro.sd.utcn.cars.Models.Car;

import java.util.List;

@Service
public class CarService {

    @Autowired
    CarDAO carDAO;

    public Car findCar(String model) {
        return carDAO.findByModel(model);
    }

    public List<Car> findAllCars() {
        return carDAO.findAll();
    }

    public void saveCar(Car car) {
        carDAO.save(car);
    }

    public void deleteCar(Car car) {
        carDAO.delete(car);
    }
}
