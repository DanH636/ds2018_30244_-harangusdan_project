package ro.sd.utcn.cars.BLL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ro.sd.utcn.cars.DAO.UserDAO;
import ro.sd.utcn.cars.Models.User;

import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    UserDAO userDAO;

    public User findUser(String username, String password){
        PasswordEncoder encoder = new BCryptPasswordEncoder(10);
        System.out.println(password + " " + encoder.encode(password));
        // return userDAO.findByUsernameAndPassword(username, encoder.encode(password));
        return userDAO.findAll().stream().filter(user -> encoder.matches(password, user.getPassword()) && user.getUsername().equals(username)).findFirst().orElse(null);
    }

    public void saveUser(User user){
        userDAO.save(user);
    }

    public void deleteUser(User user){
        userDAO.delete(user);
    }
}
