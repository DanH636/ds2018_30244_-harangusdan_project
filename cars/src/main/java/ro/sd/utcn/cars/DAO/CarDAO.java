package ro.sd.utcn.cars.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.sd.utcn.cars.Models.Car;

import java.util.List;

public interface CarDAO extends JpaRepository<Car, Integer> {

    public Car findByModel(String model);
}
