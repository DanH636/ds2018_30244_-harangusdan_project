package ro.sd.utcn.cars.Models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "fav_part")
public class FavoritePart implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne(targetEntity = Part.class)
    @JoinColumn(name = "part_id")
    private Part part;

    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name = "user_id")
    private User user;

    public FavoritePart() {
    }

    public FavoritePart(Part part, User user) {
        this.part = part;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Part getPart() {
        return part;
    }

    public void setPart(Part part) {
        this.part = part;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "FavoritePart{" +
                "id=" + id +
                ", part=" + part +
                ", user=" + user +
                '}';
    }
}
