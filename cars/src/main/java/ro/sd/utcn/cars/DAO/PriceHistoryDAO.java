package ro.sd.utcn.cars.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.sd.utcn.cars.Models.Car;
import ro.sd.utcn.cars.Models.CarPriceHistory;

import java.util.List;

public interface PriceHistoryDAO extends JpaRepository<CarPriceHistory,Integer> {

    public List<CarPriceHistory> findAllByCarId_Model(String model);
}
