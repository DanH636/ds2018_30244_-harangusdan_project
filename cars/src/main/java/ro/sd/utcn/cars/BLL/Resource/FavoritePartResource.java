package ro.sd.utcn.cars.BLL.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.sd.utcn.cars.BLL.FavoritePartService;
import ro.sd.utcn.cars.Models.FavoritePart;

import java.util.List;

@RestController
@RequestMapping(value = "/favoritesParts")
public class FavoritePartResource {

    @Autowired
    FavoritePartService favoritePartService;

    @GetMapping(value = "/{username}", produces = "application/json")
    public List<FavoritePart> getAllFavoriteParts(@PathVariable("username") String username) {
        return favoritePartService.findAllByUser(username);
    }

    @DeleteMapping(value = "/{part}", produces = "application/json")
    public void deleteFavoriteCar(@PathVariable("part") String part) {
        FavoritePart favoritePart = favoritePartService.findByPart(part);
        favoritePartService.deleteFavoritePart(favoritePart);
    }

    @PutMapping(value = "/fav", produces = "application/json")
    public void putFavoritePart(@RequestBody FavoritePart favoritePart) {
        favoritePartService.addFavoritePart(favoritePart);
    }
}
