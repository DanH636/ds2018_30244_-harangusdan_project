package ro.sd.utcn.cars.Models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "car_deals")
public class CarDeal implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name="owner_user")
    private User ownerUser;

    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name="deal_user")
    private User dealUser;

    @Column(name = "deal_amount")
    private int dealAmount;

    @ManyToOne(targetEntity = Car.class)
    @JoinColumn(name="car_id")
    private Car carId;

    public CarDeal() {
    }

    public CarDeal(User ownerUser, User dealUser, int dealAmount, Car carId) {
        this.ownerUser = ownerUser;
        this.dealUser = dealUser;
        this.dealAmount = dealAmount;
        this.carId = carId;
    }

    public User getOwnerUser() {
        return ownerUser;
    }

    public void setOwnerUser(User ownerUser) {
        this.ownerUser = ownerUser;
    }

    public User getDealUser() {
        return dealUser;
    }

    public void setDealUser(User dealUser) {
        this.dealUser = dealUser;
    }

    public int getDealAmount() {
        return dealAmount;
    }

    public void setDealAmount(int dealAmount) {
        this.dealAmount = dealAmount;
    }

    public Car getCarId() {
        return carId;
    }

    public void setCarId(Car carId) {
        this.carId = carId;
    }

    @Override
    public String toString() {
        return "CarDeal{" +
                "ownerUser=" + ownerUser +
                ", dealUser=" + dealUser +
                ", dealAmount=" + dealAmount +
                ", carId=" + carId +
                '}';
    }
}
